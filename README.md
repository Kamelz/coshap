# CoshapBackend

## Installation

- copy&paste .env.example file and  rename it .env

- run commands `composer install`

- `php artisan migrate`

- `php artisan db:seed`

- `php artisan serve`


