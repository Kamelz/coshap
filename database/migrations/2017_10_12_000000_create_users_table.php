<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('phone');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');

            $table->integer('role_id')->unsigned()->nullable();
            $table->foreign('role_id')
                ->references('id')->on('roles')
                ->default(null)
                ->onDelete('cascade');


            $table->integer('status_id')->unsigned()->nullable();
            $table->foreign('status_id')
                ->references('id')->on('status')
                ->default(null)
                ->onDelete('cascade');


            $table->integer('contract_id')->unsigned()->nullable();
            $table->foreign('contract_id')
                ->references('id')->on('contract')
                ->onDelete('cascade');


            $table->integer('package_id')->unsigned()->nullable();
            $table->foreign('package_id')
                ->references('id')->on('packages')
                ->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
