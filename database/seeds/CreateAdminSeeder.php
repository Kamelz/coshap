<?php

use App\Role;
use App\User;
use App\Status;
use App\Contract;
use Illuminate\Database\Seeder;

class CreateAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role= Role::where('role','admin')->get()->first();
        
                $status= Status::where('status','Active')->first();
        
                $contract= Contract::first();
                $user = User::create([
                    'name' => "Admin",
                    'email' => "admin@admin.com",
                    'phone' => 0112212,
                    'role_id' => $role->id,
                    'status_id' => $status->id,
                    'contract_id' => $contract->id,
                    'password' => bcrypt(123456),
                ]);
    }
}
